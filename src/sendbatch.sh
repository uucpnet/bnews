: '@(#)sendbatch.sh	1.22	10/29/89'

LIB=LIBDIR
BATCH=BATCHDIR
LIM=50000
MAXBATCH=MAXPERBATCH
RNEWS=rnews
UUX_FLAGS="UUXFLAGS"
CMD='$LIB/batch $BATCH/$rmt $BLIM'
ECHO=
COMP=
C7=
cflags=
DOIHAVE=
SPOOLDISK=SPOOL_DISK
MINDF=MINDISKFREE

for rmt
do
	case $rmt in
	-s*)	LIM=`expr "$rmt" : '-s\(.*\)'`
		continue;;
	-m*)	MAXBATCH=`expr "$rmt" : '-m\(.*\)'`
		continue;;
	+m*)	MAXBATCH=''
		continue;;
	-p*)	MAXPERRUN=`expr "$rmt" : '-p\(.*\)'`
		continue;;
	+p*)	MAXPERRUN=''
		continue;;
	-r*)	RNEWS=`expr "$rmt" : '-r\(.*\)'`
		continue;;
	-u*)	UUX_FLAGS=`expr "$rmt" : '-u\(.*\)'`
		continue;;
	-c7) 	COMP='| $LIB/compress $cflags'
		C7='| $LIB/encode'
		ECHO='echo "#! c7unbatch"'
		continue;;
	-c)	COMP='| $LIB/compress $cflags'
		ECHO='echo "#! cunbatch"'
		continue;;
	+c*)	COMP=''
		C7=''
		ECHO=''
		cflags=''
		continue;;
	-[bBC]*)	cflags="$cflags $rmt"; continue;;
	-o*)	ECHO=`expr "$rmt" : '-o\(.*\)'`
		RNEWS='cunbatch'
		continue;;
	+o*)	ECHO=''
		RNEWS=rnews
		continue;;
	-i*)	DOIHAVE=`expr "$rmt" : '-i\(.*\)'`
		if test -z "$DOIHAVE"
		then
			DOIHAVE=`uuname -l`
		fi
		continue;;
	+i*)	DOIHAVE=''
		continue;;
	-D*)	SPOOLDISK=`expr "$rmt" : '-D\(.*\)'`
		continue;;
	+D*)	SPOOLDISK=''
		continue;;
	-f*)	MINDF=`expr "$rmt" : '-f\(.*\)'`
		continue;;
	esac
	if test ! -n "$MAXPERRUN"
	then
		MAXPERRUN=$MAXBATCH
	fi

	if test -n "$SPOOLDISK"
	then
		df=`df $SPOOLDISK | awk '
			$6 == "'$SPOOLDISK'" {print $4;exit}
			$1 == "'$SPOOLDISK'" && NF == 7 {print $4;exit}
			$2 == "'$SPOOLDISK'" {print $5;exit}
			$1 == "'$SPOOLDISK'" {print $3;exit}
			'`
		if test ! -z "$df" -a \( "$df" -lt $MINDF \)
		then
			echo $rmt: not enough space on $SPOOLDISK: $df
			continue
		fi
	fi

	if test -n "$MAXBATCH" -a -d /usr/spool/uucp/$rmt
	then
		du=`du "/usr/spool/uucp/$rmt" | sed 's/	.*/000/'`
		if test ! -z "$du" -a \( "$du" -gt $MAXBATCH \)
		then 
			echo $rmt already has $du Kbytes queued
			continue
		fi
	fi

	if test -n "$COMP"
	then
		BLIM=`expr $LIM \* 2`
	else
		BLIM=$LIM
	fi

	# only do ihave processing once, not every time through loop
	if test -n "$DOIHAVE" -a -s BATCHDIR/$rmt.ihave
	then
		mv $BATCH/$rmt.ihave $BATCH/$rmt.$$
		$LIB/inews -t "cmsg ihave $DOIHAVE" -n to.$rmt.ctl < \
			$BATCH/$rmt.$$
		rm $BATCH/$rmt.$$
	fi

	# make sure $? is zero
	sentbytes=0
	bytes_this_batch=`expr $MAXBATCH - $du`
	if test $bytes_this_batch -gt $MAXPERRUN
	then
		bytes_this_batch=$MAXPERRUN
	fi
	while test $? -eq 0 -a $sentbytes -le $bytes_this_batch -a \
		\( \( $sentbytes -eq 0 -a -s $BATCH/$rmt \) -o \
		 -s $BATCH/$rmt.work \)
	do
		(eval $ECHO; eval $CMD $COMP $C7) |
		if test -s $BATCH/$rmt.cmd
		then
			$BATCH/$rmt.cmd
		else
			uux - $UUX_FLAGS $rmt!$RNEWS
		fi
		sentbytes=`expr $sentbytes + $LIM`
	done
done
