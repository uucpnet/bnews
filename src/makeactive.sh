: "Create active file and newsgroup hierarchy for new machine"
: "Usage: sh makeactive.sh LIBDIR SPOOLDIR NEWSUSR NEWSGRP"
: '@(#)makeactive	1.29	9/7/89'
LIBDIR=$1
SPOOLDIR=$2
NEWSUSR=$3
NEWSGRP=$4
cat <<"E_O_F" | sort > /tmp/$$groups
comp.ai			Artificial intelligence discussions.
comp.ai.digest		Artificial Intelligence discussions. (Moderated)
comp.ai.edu		Applications of Artificial Intelligence to Education.
comp.ai.neural-nets	All aspects of neural networks.
comp.ai.nlang-know-rep	Natural Language and Knowledge Representation. (Moderated)
comp.ai.shells		Artificial intelligence applied to shells. (Moderated)
comp.ai.vision		Artificial Intelligence Vision Research. (Moderated)
comp.arch		Computer architecture.
comp.archives		Descriptions of public access archives. (Moderated)
comp.binaries.amiga	Encoded public domain programs in binary. (Moderated)
comp.binaries.apple2	Binary-only postings for the Apple II computer.
comp.binaries.atari.st	Binary-only postings for the Atari ST. (Moderated)
comp.binaries.ibm.pc	Binary-only postings for IBM PC/MS-DOS. (Moderated)
comp.binaries.ibm.pc.d	Discussions about IBM/PC binary postings.
comp.binaries.mac	Encoded Macintosh programs in binary. (Moderated)
comp.bugs.2bsd		Reports of UNIX* version 2BSD related bugs.
comp.bugs.4bsd		Reports of UNIX version 4BSD related bugs.
comp.bugs.4bsd.ucb-fixes	Bug reports/fixes for BSD Unix. (Moderated)
comp.bugs.misc		General UNIX bug reports and fixes (incl V7, uucp)
comp.bugs.sys5		Reports of USG (System III, V, etc.) bugs.
comp.cog-eng		Cognitive engineering.
comp.compilers		Compiler construction, theory, etc. (Moderated)
comp.databases		Database and data management issues and theory.
comp.dcom.lans		Local area network hardware and software.
comp.dcom.lans.hyperchannel	Hyperchannel networks within an IP network.
comp.dcom.lans.v2lni	Proteon Pronet/V2LNI Ring networks.
comp.dcom.modems	Data communications hardware and software.
comp.dcom.telecom	Telecommunications digest. (Moderated)
comp.doc		Archived public-domain documentation. (Moderated)
comp.doc.techreports	Lists of technical reports. (Moderated)
comp.editors		Topics related to computerized text editing.
comp.edu		Computer science education.
comp.edu.composition	Writing instruction in computer-based classrooms.
comp.emacs		EMACS editors of different flavors.
comp.fonts		Typefonts -- design, conversion, use, etc.
comp.graphics		Computer graphics, art, animation, image processing.
comp.graphics.digest	Graphics software, hardware, theory, etc. (Moderated)
comp.ivideodisc		Interactive videodiscs -- uses, potential, etc.
comp.lang.ada		Discussion about Ada*.
comp.lang.apl		Discussion about APL.
comp.lang.asm370	Programming in IBM System/370 Assembly Language.
comp.lang.c		Discussion about C.
comp.lang.c++		The object-oriented C++ language.
comp.lang.clu		The CLU language & related topics. (Moderated)
comp.lang.eiffel	The object-oriented Eiffel language.
comp.lang.forth		Discussion about Forth.
comp.lang.forth.mac	The CSI MacForth programming environment.
comp.lang.fortran	Discussion about FORTRAN.
comp.lang.icon		Topics related to the ICON programming language.
comp.lang.idl		IDL (Interface Description Language) related topics.
comp.lang.lisp		Discussion about LISP.
comp.lang.lisp.franz	The Franz Lisp programming language.
comp.lang.lisp.x	The XLISP language system.
comp.lang.misc		Different computer languages not specifically listed.
comp.lang.modula2	Discussion about Modula-2.
comp.lang.pascal	Discussion about Pascal.
comp.lang.postscript	The PostScript Page Description Language.
comp.lang.prolog	Discussion about PROLOG.
comp.lang.rexx		The REXX command language.
comp.lang.scheme	The Scheme Programming language.
comp.lang.scheme.c	The Scheme language environment.
comp.lang.sigplan	Info & announcements from ACM SIGPLAN. (Moderated)
comp.lang.smalltalk	Discussion about Smalltalk 80.
comp.lang.visual	Visual programming languages.
comp.laser-printers	Laser printers, hardware & software. (Moderated)
comp.lsi		Large scale integrated circuits.
comp.lsi.cad		Electrical Computer Aided Design.
comp.mail.elm		Discussion and fixes for ELM mail system. 
comp.mail.headers	Gatewayed from the ARPA header-people list.
comp.mail.maps		Various maps, including UUCP maps. (Moderated)
comp.mail.mh		The UCI version of the Rand Message Handling system.
comp.mail.misc		General discussions about computer mail.
comp.mail.multi-media	Multimedia Mail.
comp.mail.mush		The Mail User's Shell (MUSH).
comp.mail.sendmail	Configuring and using the BSD sendmail agent.
comp.mail.uucp		Mail in the uucp network environment.
comp.misc		General topics about computers not covered elsewhere.
comp.music		Applications of computers in music research.
comp.newprod		Announcements of new products of interest. (Moderated)
comp.org.decus		DEC* Users' Society newsgroup.
comp.org.fidonet	FidoNews digest, official news of FidoNet Assoc. (Moderated)
comp.org.ieee		Issues and announcements about the IEEE & its members.
comp.org.usenix		USENIX Association events and announcements.
comp.org.usrgroup	News/discussion about/from the /usr/group organization.
comp.os.aos		Topics related to Data General's AOS/VS.
comp.os.cpm		Discussion about the CP/M operating system.
comp.os.cpm.amethyst	Discussion of Amethyst, CP/M-80 software package.
comp.os.eunice		The SRI Eunice system.
comp.os.mach		The MACH OS from CMU & other places.
comp.os.minix		Discussion of Tanenbaum's MINIX system.
comp.os.misc		General OS-oriented discussion not carried elsewhere.
comp.os.os9		Discussions about the os9 operating system.
comp.os.research	Operating systems and related areas. (Moderated)
comp.os.rsts		Topics related to the PDP-11 RSTS/E operating system.
comp.os.v		The V distributed operating system from Stanford.
comp.os.vms		DEC's VAX* line of computers & VMS.
comp.os.xinu		The XINU operating system from Purdue (D. Comer).
comp.parallel		Massively parallel hardware/software. (Moderated)
comp.periphs		Peripheral devices.
comp.periphs.printers	Information on printers.
comp.protocols.appletalk	Applebus hardware & software.
comp.protocols.ibm	Networking with IBM mainframes.
comp.protocols.iso	The ISO protocol stack.
comp.protocols.iso.dev-environ	The ISO Development Environment.
comp.protocols.iso.x400	X400 mail protocol discussions.  (Moderated)
comp.protocols.iso.x400.gateway	X400 mail gateway discussions.  (Moderated)
comp.protocols.kerberos	The Kerberos authentification server.
comp.protocols.kermit	Info about the Kermit package. (Moderated)
comp.protocols.misc	Various forms and types of FTP protocol.
comp.protocols.nfs	Discussion about the Network File System protocol.
comp.protocols.pcnet	Topics related to PCNET (a personal computer network).
comp.protocols.pup	The Xerox PUP network protocols.
comp.protocols.tcp-ip	TCP and IP network protocols.
comp.protocols.tcp-ip.domains	Topics related to Domain Style names.
comp.protocols.tcp-ip.ibmpc	TCP/IP for IBM(-like) personal computers.
comp.realtime		Issues related to real-time computing.
comp.risks		Risks to the public from computers & users. (Moderated)
comp.simulation		Simulation methods, problems, uses. (Moderated)
comp.society		The impact of technology on society. (Moderated)
comp.society.futures	Events in technology affecting future computing.
comp.society.women	Women's roles and problems in computing (Moderated)
comp.soft-sys.andrew	The Andrew system from CMU.
comp.software-eng	Software Engineering and related topics.
comp.sources.amiga	Source code-only postings for the Amiga. (Moderated)
comp.sources.atari.st	Source code-only postings for the Atari ST. (Moderated)
comp.sources.bugs	Bug reports, fixes, discussion for posted sources
comp.sources.d		For any discussion of source postings.
comp.sources.games	Postings of recreational software. (Moderated)
comp.sources.games.bugs	Bug reports and fixes for posted game software.
comp.sources.mac	Software for the Apple Macintosh. (Moderated)
comp.sources.misc	Posting of software . (Moderated)
comp.sources.sun	Software for Sun workstations. (Moderated)
comp.sources.unix	Postings of complete, UNIX-oriented sources. (Moderated)
comp.sources.wanted	Requests for software and fixes.
comp.sources.x		Software for the X windows system. (Moderated)
comp.std.c		Discussion about C language standards.
comp.std.internat	Discussion about international standards.
comp.std.misc		Discussion about various standards.
comp.std.mumps		Discussion for the X11.1 committee on Mumps. (Moderated)
comp.std.unix		Discussion for the P1003 committee on UNIX. (Moderated)
comp.sw.components	Software components and related technology.
comp.sys.amiga		Commodore Amiga: info&uses, but no programs.
comp.sys.amiga.tech	Technical discussion about the Amiga.
comp.sys.apollo		Apollo computer systems.
comp.sys.apple		Discussion about Apple micros.
comp.sys.atari.8bit	Discussion about 8 bit Atari micros.
comp.sys.atari.st	Discussion about 16 bit Atari micros.
comp.sys.att		Discussions about AT&T microcomputers.
comp.sys.cbm		Discussion about Commodore micros.
comp.sys.cdc		Control Data Corporation Computers (e.g., Cybers).
comp.sys.celerity	Celerity Computers
comp.sys.dec		Discussions about DEC computer systems.
comp.sys.dec.micro	DEC Micros (Rainbow, Professional 350/380)
comp.sys.encore		Encore's MultiMax computers.
comp.sys.handhelds	Handheld computers and programmable calculators.
comp.sys.hp		Discussion about Hewlett-Packard equipment.
comp.sys.ibm.pc		Discussion about IBM personal computers.
comp.sys.ibm.pc.digest	The IBM PC, PC-XT, and PC-AT. (Moderated)
comp.sys.ibm.pc.rt	Topics related to IBM's RT computer.
comp.sys.intel		Discussions about Intel systems and parts.
comp.sys.intel.ipsc310	Anything related to Xenix on an Intel 310.
comp.sys.isis		The ISIS distributed system from Cornell.
comp.sys.m6809		Discussion about 6809's.
comp.sys.m68k		Discussion about 68k's.
comp.sys.m68k.pc	Discussion about 68k-based PCs. (Moderated)
comp.sys.mac		Discussions about the Apple Macintosh & Lisa.
comp.sys.mac.digest	Apple Macintosh: info&uses, but no programs. (Moderated)
comp.sys.mac.hypercard	The Macintosh Hypercard: info & uses.
comp.sys.mac.programmer	Discussion by people programming the Apple Macintosh.
comp.sys.masscomp	The Masscomp line of computers. (Moderated)
comp.sys.mips		Systems based on MIPS chips.
comp.sys.misc		Discussion about computers of all kinds.
comp.sys.next		Discussion about the new NeXT computer.
comp.sys.northstar	Northstar microcomputer users.
comp.sys.nsc.32k	National Semiconductor 32000 series chips.
comp.sys.proteon	Proteon gateway products.
comp.sys.pyramid	Pyramid 90x computers.
comp.sys.ridge		Ridge 32 computers and ROS. 
comp.sys.sequent	Sequent systems, (Balance and Symmetry).
comp.sys.sgi		Silicon Graphics's Iris workstations and software.
comp.sys.sun		Sun "workstation" computers. (Moderated)
comp.sys.super		Supercomputers.
comp.sys.tahoe		CCI 6/32, Harris HCX/7, & Sperry 7000 computers.
comp.sys.tandy		Discussion about TRS-80's.
comp.sys.ti		Discussion about Texas Instruments.
comp.sys.ti.explorer	The Texas Instruments Explorer.
comp.sys.transputer	The Transputer computer and OCCAM language.
comp.sys.workstations	Various workstation-type computers. (Moderated)
comp.sys.xerox		Xerox 1100 workstations and protocols.
comp.sys.zenith		Heath terminals and related Zenith products.
comp.sys.zenith.z100	The Zenith Z-100 (Heath H-100) family of computers.
comp.terminals		All sorts of terminals.
comp.terminals.bitgraph	The BB&N BitGraph Terminal.
comp.terminals.tty5620	AT&T Dot Mapped Display Terminals (5620 and BLIT).
comp.text		Text processing issues and methods.
comp.text.desktop	Technology & techniques of desktop publishing.
comp.theory		Theoretical Computer Science.
comp.theory.cell-automata	Discussion of all aspects of cellular automata.
comp.theory.dynamic-sys	Ergodic Theory and Dynamical Systems.
comp.theory.info-retrieval	Information Retrieval topics. (Moderated)
comp.theory.self-org-sys	Topics related to self-organization.
comp.unix		Discussion of UNIX* features and bugs. (Moderated)
comp.unix.aux		The version of UNIX for Apple Macintosh II computers.
comp.unix.cray		Cray computers and their operating systems.
comp.unix.i386		Versions of Unix running on Intel 80386-based boxes.
comp.unix.microport	Discussion of Microport's UNIX.
comp.unix.questions	UNIX neophytes group.
comp.unix.ultrix	Discussions about DEC's Ultrix.
comp.unix.wizards	Discussions, bug reports, and fixes on and for UNIX.
comp.unix.xenix		Discussion about the Xenix OS.
comp.virus		Computer viruses & security. (Moderated)
comp.windows.misc	Various issues about windowing systems.
comp.windows.ms		Window systems under MS/DOS.
comp.windows.news	Sun Microsystems' NeWS window system.
comp.windows.x		Discussion about the X Window System.
misc.consumers		Consumer interests, product reviews, etc.
misc.consumers.house	Discussion about owning and maintaining a house.
misc.emerg-services	Forum for paramedics & other first responders.
misc.forsale		Short, tasteful postings about items for sale.
misc.handicap		Items of interest for/about the handicapped. (Moderated)
misc.headlines		Current interest: drug testing, terrorism, etc.
misc.headlines.unitex	International news from the UN & related. (Moderated)
misc.invest		Investments and the handling of money.
misc.jobs.misc		Discussion about employment, workplaces, careers.
misc.jobs.offered	Announcements of positions available.
misc.jobs.resumes	Postings of resumes and "situation wanted" articles.
misc.kids		Children, their behavior and activities.
misc.legal		Legalities and the ethics of law.
misc.misc		Various discussions not fitting in any other group.
misc.security		Security in general, not just computers. (Moderated)
misc.taxes		Tax laws and advice.
misc.test		For testing of network software.  Very boring.
misc.wanted		Requests for things that are needed (NOT software).
news.admin		Comments directed to news administrators.
news.announce.conferences	Calls for papers and conference announcements. (Moderated)
news.announce.important	General announcements of interest to all. (Moderated)
news.announce.newgroups	Calls for newgroups & announcements of same. (Moderated)
news.announce.newusers	Explanatory postings for new users. (Moderated)
news.config		Postings of system down times and interruptions.
news.groups		Discussions and lists of newsgroups.
news.lists		News-related statistics and lists. (Moderated)
news.misc		Discussions of USENET itself.
news.newsites		Postings of new site announcements.
news.newusers.questions	Q & A for users new to the Usenet.
news.software.anu-news	VMS B-news software from Australian National Univ.
news.software.b		Discussion about B-news-compatible software.
news.software.nntp	The Network News Transfer Protocol.
news.software.notes	Notesfile software from the Univ. of Illinois.
news.sysadmin		Comments directed to system administrators.
rec.arts.anime		Japanese animation fen discussion.
rec.arts.books		Books of all genres, and the publishing industry.
rec.arts.comics		Comic books and strips, graphic novels, sequential art.
rec.arts.drwho		Discussion about Dr. Who.
rec.arts.int-fiction	Discussions about interactive fiction.
rec.arts.misc		Discussions about the arts not in other groups.
rec.arts.movies		Discussions of movies and movie making.
rec.arts.movies.reviews	Reviews of movies. (Moderated)
rec.arts.poems		For the posting of poems.
rec.arts.sf-lovers	Science fiction lovers' newsgroup.
rec.arts.startrek	Star Trek, the TV shows and the movies.
rec.arts.tv		The boob tube, its history, and past and current shows.
rec.arts.tv.soaps	Postings about soap operas.
rec.arts.tv.uk		Discussions of telly shows from the UK.
rec.arts.wobegon	"A Prairie Home Companion" radio show discussion.
rec.audio		High fidelity audio.
rec.autos		Automobiles, automotive products and laws.
rec.autos.sport		Discussion of organized, legal auto competitions.
rec.autos.tech		Technical aspects of automobiles, et. al.
rec.aviation		Aviation rules, means, and methods.
rec.backcountry		Activities in the Great Outdoors.
rec.bicycles		Bicycles, related products and laws.
rec.birds		Hobbyists interested in bird watching.
rec.boats		Hobbyists interested in boating.
rec.equestrian		Discussion of things equestrian.
rec.folk-dancing	Folk dances, dancers, and dancing.
rec.food.cooking	Food, cooking, cookbooks, and recipes.
rec.food.drink		Wines and spirits.
rec.food.veg		Vegetarians.
rec.games.board		Discussion and hints on board games.
rec.games.bridge	Hobbyists interested in bridge.
rec.games.chess		Chess & computer chess.
rec.games.empire	Discussion and hints about Empire.
rec.games.frp		Discussion about Fantasy Role Playing games.
rec.games.go		Discussion about Go.
rec.games.hack		Discussion, hints, etc. about the Hack game.
rec.games.misc		Games and computer games.
rec.games.moria		Comments, hints, and info about the Moria game.
rec.games.pbm		Discussion about Play by Mail games.
rec.games.programmer	Discussion of adventure game programming.
rec.games.rogue		Discussion and hints about Rogue.
rec.games.trivia	Discussion about trivia.
rec.games.vectrex	The Vectrex game system.
rec.games.video		Discussion about video games.
rec.gardens		Gardening, methods and results.
rec.guns		Discussions about firearms. (Moderated)
rec.ham-radio		Amateur Radio practices, contests, events, rules, etc.
rec.ham-radio.packet	Discussion about packet radio setups.
rec.ham-radio.swap	Offers to trade and swap radio equipment.
rec.humor		Jokes and the like.  May be somewhat offensive.
rec.humor.d		Discussions on the content of rec.humor articles.
rec.humor.funny		Jokes that are funny (in the moderator's opinion).  (Moderated)
rec.mag			Magazine summaries, tables of contents, etc.
rec.mag.fsfnet		A Science Fiction "fanzine." (Moderated)
rec.mag.otherrealms	Edited science fiction & fantasy "magazine". (Moderated)
rec.misc		General topics about recreational/participant sports.
rec.models.rc		Radio-controlled models for hobbyists.
rec.motorcycles		Motorcycles and related products and laws.
rec.music.beatles	Postings about the Fab Four & their music.
rec.music.bluenote	Discussion of jazz, blues, and related types of music.
rec.music.cd		CDs -- availability and other discussions.
rec.music.classical	Discussion about classical music.
rec.music.dementia	Discussion of comedy and novelty music.
rec.music.dylan		Discussion of Bob's works & music.
rec.music.folk		Folks discussing folk music of various sorts.
rec.music.gaffa		Progressive music (e.g., Kate Bush). (Moderated)
rec.music.gdead		A group for (Grateful) Dead-heads.
rec.music.makers	For performers and their discussions.
rec.music.misc		Music lovers' group.
rec.music.newage	"New Age" music discussions.
rec.music.synth		Synthesizers and computer music.
rec.nude		Hobbyists interested in naturist/nudist activities.
rec.pets		Pets, pet care, and household animals in general.
rec.photo		Hobbyists interested in photography.
rec.puzzles		Puzzles, problems, and quizzes.
rec.railroad		Real and model train fans' newsgroup.
rec.scuba		Hobbyists interested in SCUBA diving.
rec.skiing		Hobbyists interested in snow skiing.
rec.skydiving		Hobbyists interested in skydiving.
rec.sport.baseball	Discussion about baseball.
rec.sport.basketball	Discussion about basketball.
rec.sport.football	Discussion about football.
rec.sport.hockey	Discussion about hockey.
rec.sport.misc		Spectator sports.
rec.travel		Traveling all over the world.
rec.video		Video and video components.
rec.woodworking		Hobbyists interested in woodworking.
sci.aeronautics		The science of aeronautics & related technology.
sci.astro		Astronomy discussions and information.
sci.bio			Biology and related sciences.
sci.bio.technology	Any topic relating to biotechnology.
sci.chem		Chemistry and related sciences.
sci.crypt		Different methods of data en/decryption.
sci.edu			Science education.
sci.electronics		Circuits, theory, electrons and discussions.
sci.energy		Discussions about energy, science & technology.
sci.environment		Discussions about the environment and ecology.
sci.lang		Natural languages, communication, etc.
sci.lang.japan		The Japanese language, both spoken and written.
sci.logic		Logic -- math, philosophy & computational aspects.
sci.math		Mathematical discussions and pursuits.
sci.math.num-analysis	Numerical Analysis.
sci.math.stat		Statistics discussion.
sci.math.symbolic	Symbolic algebra discussion.
sci.med			Medicine and its related products and regulations.
sci.med.aids		AIDS: treatment, pathology/biology of HIV, prevention. (Moderated)
sci.med.physics		Issues of physics in medical testing/care.
sci.military		Discussion about science & the military. (Moderated)
sci.misc		Short-lived discussions on subjects in the sciences.
sci.nanotech		Self-reproducing molecular-scale machines. (Moderated)
sci.philosophy.meta	Discussions within the scope of "MetaPhilosophy."
sci.philosophy.tech	Technical philosophy: math, science, logic, etc. 
sci.physics		Physical laws, properties, etc.
sci.physics.fusion	Info on fusion, esp. "cold" fusion.
sci.psychology		Topics related to psychology.
sci.research		Research methods, funding, ethics, and whatever.
sci.skeptic		Skeptics discussing psuedo-science.
sci.space		Space, space programs, space related research, etc.
sci.space.shuttle	The space shuttle and the STS program.
soc.college		College, college activities, campus life, etc.
soc.couples		Discussions for couples (cf. soc.singles).
soc.culture.african	Discussions about Africa & things African.
soc.culture.arabic	Technological & cultural issues, *not* politics.
soc.culture.asian.american	Issues & discussion about Asian-Americans.
soc.culture.celtic	Group about Celts (*not* basketball!).
soc.culture.china	About China and Chinese culture.
soc.culture.esperanto	The neutral international language Esperanto.
soc.culture.greek	Group about Greeks.
soc.culture.hongkong	Discussions pertaining to Hong Kong.
soc.culture.indian	Group for discussion about India & things Indian.
soc.culture.japan	Everything Japanese, except the Japanese language.
soc.culture.jewish	Jewish culture & religion. (cf. talk.politics.mideast)
soc.culture.misc	Group for discussion about other cultures.
soc.culture.nordic	Discussion about culture up north.
soc.culture.taiwan	Discussion about things Taiwanese.
soc.culture.turkish	Discussion about things Turkish.
soc.feminism		Discussion of feminism & feminist issues. (Moderated)
soc.human-nets		Computer aided communications digest. (Moderated)
soc.men			Issues related to men, their problems & relationships.
soc.misc		Socially-oriented topics not in other groups.
soc.motss		Issues pertaining to homosexuality.
soc.net-people		Announcements, requests, etc. about people on the net.
soc.politics		Political problems, systems, solutions. (Moderated)
soc.politics.arms-d	Arms discussion digest. (Moderated)
soc.religion.christian	Christianity and related topics. (Moderated)
soc.rights.human	Human rights & activism (e.g., Amnesty International).
soc.roots		Genealogical matters.
soc.singles		Newsgroup for single people, their activities, etc.
soc.women		Issues related to women, their problems & relationships.
talk.abortion		All sorts of discussions and arguments on abortion.
talk.bizarre		The unusual, bizarre, curious, and often stupid.
talk.origins		Evolution versus creationism (sometimes hot!).
talk.philosophy.misc	Philosophical musings on all topics.
talk.politics.guns	The politics of firearm ownership and (mis)use.
talk.politics.mideast	Discussion & debate over Middle Eastern events.
talk.politics.misc	Political discussions and ravings of all kinds.
talk.politics.soviet	Discussion of Soviet politics, domestic and foreign.
talk.politics.theory	Theory of politics and political systems.
talk.rape		Discussions on stopping rape; not to be crossposted.
talk.religion.misc	Religious, ethical, & moral implications.
talk.religion.newage	Esoteric and minority religions & philosophies.
talk.rumors		For the posting of rumors.
E_O_F
: if active file is empty, create it
if test ! -s $LIBDIR/active
then
	sed 's/[ 	].*/ 00000 00001/' /tmp/$$groups > $LIBDIR/active
	cat <<'E_O_F' >>$LIBDIR/active
control 0000000 0000000
junk 0000000 0000000
E_O_F
	set - group 0 1
else
: increase max article number if necessary
sed 's/ \([0-9][0-9][0-9][0-9][0-9]\) \([0-9][0-9][0-9][0-9][0-9]\) / 00\1 00\2 /' $LIBDIR/active>$LIBDIR/nactive && mv $LIBDIR/nactive $LIBDIR/active
: make sure it is in the new format
	set - `sed 1q $LIBDIR/active`
	case $# in
	4)	ed - $LIBDIR/active << 'EOF'
g/^mod\./s/y$/m/
w
q
EOF
		;;
	3)	;;
	2)	ed - $LIBDIR/active << 'EOF'
1,$s/$/ 0000000/
w
q
EOF
		echo
		echo Active file updated to new format.
		echo You must run expire immediately after this install
		echo is done to properly update the tables.;;
	*) echo Active file is in unrecognized format. Not upgraded.;;
	esac
fi
if test $# -eq 3 -o $# -eq 2
then
	(sed '/^!net/!d
s/^!//
s!^!/!
s!$! /s/$/ n/!
' $LIBDIR/ngfile
	echo '/ n$/!s/$/ y/') >/tmp/$$sed
	mv $LIBDIR/active $LIBDIR/oactive
	sed -f /tmp/$$sed $LIBDIR/oactive >$LIBDIR/active
	chown $NEWSUSR $LIBDIR/active
	chgrp $NEWSGRP $LIBDIR/active
	chmod 644 $LIBDIR/active
fi
cat << EOF | cat - /tmp/$$groups | $LIBDIR/checkgroups | tee /tmp/checkgroups.out
From: make update

EOF
echo the output of checkgroups has been copied into /tmp/checkgroups.out
rm -f /tmp/$$*

nlines=`wc -l < $LIBDIR/active`
set - `grep '^#define LINES' defs.h`
if test $nlines -gt $3
then
	echo "WARNING: active file has more than $3 lines"
	echo "Increase LINES in defs.h"
fi
