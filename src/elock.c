#ifndef lint
static char	*SccsId = "@(#)elock.c	1.3	1/19/89";
#endif /* !lint */

#include "params.h"
#include <errno.h>

#ifdef BSD4_2
# include <sys/file.h>
#endif /* BSD4_2 */

#ifdef LOCKF
#include <unistd.h>
#endif /* LOCKF */

#if defined(BSD4_2) || defined(LOCKF)
static int LockFd = -1;
#endif

extern int errno;

dolock()
{
	/* set up exclusive locking so inews does not run while expire does */
#if defined(BSD4_2) || defined(LOCKF)
	LockFd = open(ACTIVE, 2);
# ifdef	LOCKF
	if (lockf(LockFd, F_LOCK, 0L) < 0)
# else	/* BSD4_2 */
	if (flock(LockFd, LOCK_EX) < 0)
# endif	/* BSD4_2 */
		xerror("Can't get lock on active file for %s : %s",
			Progname, errmsg(errno));
#else	/* !BSD4_2 && !LOCKF */
	int i = 0;
	char afline[MBUFLEN];
	sprintf(afline,"%s.lock", ACTIVE);
	while (LINK(ACTIVE, afline) < 0 && errno == EEXIST) {
		if (i++ > 5) {
			xerror("Can't get lock on active file for %s: %s",
				Progname, errmsg(errno));
		}
		sleep(i*2);
	}
#endif	/* !BSD4_2  && !LOCKF */
}

rmlock()
{
#if defined(BSD4_2) || defined(LOCKF)
	close(LockFd);
#else
	sprintf(bfr, "%s.lock", ACTIVE);
	(void) UNLINK(bfr);
#endif	/* !BSD4_2 */
}
